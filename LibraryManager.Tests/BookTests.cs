﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LibraryManager.Tests
{
    [TestClass]
    public class BookTests
    {
        private LibraryDB _sut;

        [TestInitialize]
        public void Setup()
        {
            _sut = new LibraryDB();
        }

        /// <summary>
        /// Test updating an existing book record.
        /// </summary>
        [TestMethod]
        public void UpdateBookTest()
        {
            var book = new Book("Some Book", 1, 10);
            _sut.UpdateBook(book);

            book.setTitle("Some New Title");
            book.setHolderID(2);
            var updatedBook = _sut.UpdateBook(book);

            Assert.AreEqual("Some New Title", updatedBook.getTitle());
            Assert.AreEqual(2, updatedBook.getHolderID());
        }

        /// <summary>
        /// Test updating a nonexisting book record will add a new book record.
        /// </summary>
        [TestMethod]
        public void UpdateNonexistantBookTest()
        {
            var book = new Book("Some Book", 1, 10);

            var updatedBook = _sut.UpdateBook(book);

            Assert.IsNotNull(updatedBook);
        }
    }
}
