﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManager
{
    public class Librarian
    {
        private LibraryDB m_LibraryDB;

        public Librarian(LibraryDB libraryDB)
        {
            m_LibraryDB = libraryDB;
        }

        int ID;
        LibraryDB libDB = new LibraryDB();
        public void librarianMain()
        {
            bool isDone = false;
            Book b = new Book("title", 1, 10);
            libDB.books = new List<Book>();
            libDB.books.Add(b);
            while (!isDone)
            {
                Console.Clear();
                Console.WriteLine("To add a book, press 1");
                Console.WriteLine("To remove a book, press 2");
                Console.WriteLine("To order a book, press 3");
                Console.WriteLine("To search for a book, press 4");
                Console.WriteLine("To exit, press 5");
                ConsoleKeyInfo pressed = Console.ReadKey();
                if (pressed.Key == ConsoleKey.D1)
                {
                    Console.Write("Enter the title of the new book: ");
                    String title = Console.ReadLine();
                    Console.Write("Enter the unique ID for the book: ");
                    String strId = Console.ReadLine();
                    int bookId;
                    try
                    {
                         bookId = Int32.Parse(strId);
                    }
                    catch (Exception ex)
                    {
                        if (ex is ArgumentNullException || ex is FormatException || ex is OverflowException)
                        {
                            Console.WriteLine("Invalid number entered for unique ID.");
                            continue;
                        }

                        throw ex;
                    }
                    Console.Write("Enter the max checkout time for the book: ");
                    String strTime = Console.ReadLine();
                    int bookTime;
                    try
                    {
                        bookTime = Int32.Parse(strTime);
                    }
                    catch (Exception ex)
                    {
                        if (ex is ArgumentNullException || ex is FormatException || ex is OverflowException)
                        {
                            Console.WriteLine("Invalid number entered for the max checkout time.");
                            continue;
                        }

                        throw ex;
                    }
                    if (!addBook(title, bookId, bookTime))
                    {
                        Console.WriteLine("Book could not be added as the ID is already in the database.");
                    }
                    Console.WriteLine("Book successfully added to database.");
                }
                else if (pressed.Key == ConsoleKey.D2)
                {

                }
                else if (pressed.Key == ConsoleKey.D3)
                {

                }
                else if (pressed.Key == ConsoleKey.D4)
                {
                    search();
                }
                else if (pressed.Key == ConsoleKey.D5)
                {
                    isDone = true;
                }
            }
        }

        void search()
        {
            Console.WriteLine("What is the id of the book you would like?");
            int id = int.Parse(Console.ReadLine());
            int success = 0;
            foreach (Book book in libDB.books)
            {
                if (book.getID() == id)
                {
                    success = 1;
                    Console.WriteLine("The title is " + book.getTitle());
                    Console.WriteLine("The id is " + book.getID());
                    Console.WriteLine("The status is " + book.getStatus());
                    Console.WriteLine("The holder id is " + book.getHolderID());
                    Console.WriteLine("The time checked out is " + book.getTimeCheckedOut());
                }
            }

            if (success == 0)
            {
                Console.WriteLine("Book id not found");
            }

            Console.WriteLine("Press 1 to search for another book or 2 to go back");
            ConsoleKeyInfo pressed = Console.ReadKey();
            if (pressed.Key == ConsoleKey.D1)
            {
                search();
            }
            else if (pressed.Key == ConsoleKey.D2)
            {
                librarianMain();
            }
        }

        public bool addBook(String title, int bookId, int checkoutMax)
        {
            Book newBook = new Book(title, bookId, checkoutMax);
            foreach (Book book in m_LibraryDB.getBooks())
            {
                if (book.getID().Equals(bookId))
                {
                    return false;
                }
            }
            m_LibraryDB.getBooks().Add(newBook);
            return true;
        }
    }
}
