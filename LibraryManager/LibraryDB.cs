﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManager
{
    /// <summary>
    /// LibraryDB.
    /// </summary>
    public class LibraryDB
    {
        List<Customer> customers;
        public List<Book> books;
        List<Librarian> librarians;

        /// <summary>
        /// LibraryDB constructor.
        /// </summary>
        public LibraryDB()
        {
            customers = new List<Customer>();
            books = new List<Book>();
            librarians = new List<Librarian>();
        }

        /// <summary>
        /// Update book record.
        /// </summary>
        /// <param name="updatedBook">Changes to apply to book.</param>
        /// <returns>Returns updated book record.</returns>
        public Book UpdateBook(Book updatedBook)
        {
            //Find book in list.
            var oldBook = books.SingleOrDefault(b => b.getID() == updatedBook.getID());

            //Update book record.
            oldBook = updatedBook;

            return oldBook;
        }

        public List<Book> getBooks()
        {
            return books;
        }
    }
}
