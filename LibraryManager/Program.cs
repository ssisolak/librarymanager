﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManager
{
    class Program
    {
        static void Main(string[] args)
        {
            LibraryDB libDB = new LibraryDB();
            Console.WriteLine("To Login as customer, press 1");
            Console.WriteLine("To Login as librarian, press 2");
            Console.WriteLine("Press any other key to exit");
            ConsoleKeyInfo choice = Console.ReadKey();
            Console.Clear();
            if (choice.Key == ConsoleKey.D1)
            {
                Console.WriteLine("Please enter your name: ");
                string name = Console.ReadLine();
                Console.WriteLine("Please enter your ID: ");
                int ID = Int32.Parse(Console.ReadLine());
            } 
            else if (choice.Key == ConsoleKey.D2) 
            {
                Console.WriteLine("Please enter your ID: ");
                int ID = Int32.Parse(Console.ReadLine());
                Librarian librarian = new Librarian(libDB);
                librarian.librarianMain();
            } 
            else 
            {
                Console.WriteLine("Now exiting...");
            }
        }
    }
}
