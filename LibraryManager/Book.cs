﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryManager
{
    public enum status { AVAILIBLE, ONHOLD, CHECKEDOUT, NOTAVAILIBLE, OVERDUE };

    public class Book
    {
        string title;
        status bookStatus;
        private int ID;
        int holderID;
        int timeCheckedOut;
        int maxTimeCheckedOut;

        public Book(string title, int ID, int maxCheckout)
        {
            this.title = title;
            this.ID = ID;
            this.maxTimeCheckedOut = maxCheckout;
            bookStatus = status.AVAILIBLE;
            timeCheckedOut = 0;
            holderID = -1;
        }

        public int getID()
        {
            return ID;
        }

        public int getHolderID()
        {
            return holderID;
        }

        public status getStatus()
        {
            return bookStatus;
        }

        public int getTimeCheckedOut()
        {
            return timeCheckedOut;
        }

        public string getTitle()
        {
            return title;
        }

        public void setStatus(status newStatus)
        {
            bookStatus = newStatus;
        }

        public void setTitle(string title)
        {
            this.title = title;
        }

        public void setHolderID(int ID)
        {
            holderID = ID;
        }

        public void setTimeCheckedOut(int time)
        {
            timeCheckedOut = time;
        }
    }
}
