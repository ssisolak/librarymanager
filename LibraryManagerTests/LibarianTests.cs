﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LibraryManager;

namespace LibraryManagerTests
{
    [TestClass]
    public class LibarianTests
    {
        private Librarian m_SUT;
        private LibraryDB m_Database;

        [TestInitialize]
        public void before()
        {
            m_Database = new LibraryDB();
            m_SUT = new Librarian(m_Database);
        }

        [TestMethod]
        public void TestAddBook()
        {
            bool success = m_SUT.addBook("test", 1, 5);
            Assert.AreEqual(success, true);

            //try adding second time.
            success = m_SUT.addBook("test", 1, 5);
            //should fail as ID is already in DB.
            Assert.AreEqual(success, false);
        }
    }
}
